package com.yogaadi.dompet.model.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.yogaadi.dompet.model.Wallet
import com.yogaadi.dompet.model.repository.dao.WalletDAO

class WalletRepository(private val walletDAO: WalletDAO){
    val allWallet : LiveData<List<Wallet>> = walletDAO.getAll()

    @WorkerThread
    suspend fun insert(wallet: Wallet){
        walletDAO.insertAll(wallet)
    }
}