package com.yogaadi.dompet.model.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.yogaadi.dompet.model.Transaction
import com.yogaadi.dompet.model.repository.dao.TransactionDAO

class TransactionRepository(private val transactionDAO: TransactionDAO){
    val allTransaction : LiveData<List<Transaction>> = transactionDAO.getAll()

    @WorkerThread
    suspend fun insert(transaction: Transaction){
        transactionDAO.insertAll(transaction)
    }
}