package com.yogaadi.dompet.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Transaction(
    @PrimaryKey
    @ColumnInfo(name = "id_transaksi") val idTransaksi : String,
    @ColumnInfo(name = "tanggal_transaksi") val tanggalTransaksi : String?,
    @ColumnInfo(name = "nominal") val nominal : String?,
    @ColumnInfo(name = "keterangan") val keterangan : String?,
    @ColumnInfo(name = "jenis") val jenis : String?,
    @ColumnInfo(name = "wallet_id") val walletId : Int?
)
