package com.yogaadi.dompet.model.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.yogaadi.dompet.model.Transaction
import com.yogaadi.dompet.model.User
import com.yogaadi.dompet.model.Wallet
import com.yogaadi.dompet.model.repository.dao.TransactionDAO
import com.yogaadi.dompet.model.repository.dao.UserDAO
import com.yogaadi.dompet.model.repository.dao.WalletDAO
import kotlinx.coroutines.CoroutineScope

@Database(entities = [User::class,Wallet::class,Transaction::class], version = 3)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao() : UserDAO
    abstract fun walletDAO() : WalletDAO
    abstract fun transactionDAO() : TransactionDAO

    companion object {
        @Volatile
        private var INSTANCE : AppDatabase? = null

        fun getDatabase(context : Context, scope : CoroutineScope) : AppDatabase{
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(context.applicationContext,
                    AppDatabase::class.java,
                    "db-dompet").build()

                INSTANCE = instance
                return instance
            }
        }
    }
}