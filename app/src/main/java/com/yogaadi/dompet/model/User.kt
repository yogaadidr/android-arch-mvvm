package com.yogaadi.dompet.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey
    @ColumnInfo(name = "username") val username : String,
    @ColumnInfo(name = "password") val password : String?,
    @ColumnInfo(name = "nama_lengkap") val namaLengkap : String?

)