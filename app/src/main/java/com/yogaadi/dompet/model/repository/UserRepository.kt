package com.yogaadi.dompet.model.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.yogaadi.dompet.model.User
import com.yogaadi.dompet.model.repository.dao.UserDAO

class UserRepository(private val userDao : UserDAO){
    val allUser : LiveData<List<User>> = userDao.getAll()

    @WorkerThread
    suspend fun insert(user : User){
        userDao.insertAll(user)
    }
}