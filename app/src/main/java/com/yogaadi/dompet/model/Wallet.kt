package com.yogaadi.dompet.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Wallet(
    @PrimaryKey val uid : Int,
    @ColumnInfo(name = "wallet_name") val walletName: String,
    @ColumnInfo(name = "icon") val icon : Int?,
    @ColumnInfo(name = "created_time") val createdTime: String

)