package com.yogaadi.dompet.model.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.yogaadi.dompet.model.User
import com.yogaadi.dompet.model.Wallet

@Dao
interface WalletDAO{
    @Query("Select * From wallet")
    fun getAll() : LiveData<List<Wallet>>

    @Query("SELECT * FROM wallet WHERE uid = :id LIMIT 1")
    fun findById(id: Int): Wallet

    @Insert
    suspend fun insertAll(vararg wallet: Wallet)

    @Delete
    fun delete(wallet: Wallet)

}