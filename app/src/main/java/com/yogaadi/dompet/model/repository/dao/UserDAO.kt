package com.yogaadi.dompet.model.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.yogaadi.dompet.model.User

@Dao
interface UserDAO{
    @Query("Select * From user")
    fun getAll() : LiveData<List<User>>

    @Query("Select * From user Where username in (:username)")
    fun getAllByUsername(username : Array<String>) : List<User>

    @Query("SELECT * FROM user WHERE username = :username LIMIT 1")
    fun findByUsername(username: String): User

    @Insert
    suspend fun insertAll(vararg users: User)

    @Delete
    fun delete(user: User)

}