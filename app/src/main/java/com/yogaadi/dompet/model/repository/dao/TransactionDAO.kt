package com.yogaadi.dompet.model.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.yogaadi.dompet.model.Transaction

@Dao
interface TransactionDAO{
    @Query("Select * From `transaction`")
    fun getAll() : LiveData<List<Transaction>>

    @Query("Select * From `transaction` Where id_transaksi = :idTransaksi")
    fun getAllById(idTransaksi : String) : List<Transaction>

    @Query("SELECT * FROM `transaction` WHERE jenis = :jenis")
    fun findByJenis(jenis : String) : List<Transaction>

    @Query("SELECT * FROM `transaction` WHERE wallet_id = :walletId")
    fun findByWallet(walletId : Int) : List<Transaction>

    @Query("SELECT * FROM `transaction` WHERE wallet_id = :walletId and jenis = :jenis")
    fun findJenisByWallet(walletId: Int,jenis : String) : List<Transaction>

    @Insert
    suspend fun insertAll(vararg transactions: Transaction)

    @Delete
    fun delete(transactions: Transaction)

}