package com.yogaadi.dompet.viewmodel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yogaadi.dompet.R
import com.yogaadi.dompet.model.User

class UserAdapter internal constructor(
    context : Context
) : RecyclerView.Adapter<UserAdapter.UserHolder>(){

    private val inflater : LayoutInflater = LayoutInflater.from(context)
    private var users = emptyList<User>()

    inner class UserHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val userItemView : TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAdapter.UserHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return UserHolder(itemView)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    internal fun setUsers(words: List<User>) {
        this.users = words
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: UserAdapter.UserHolder, position: Int) {
        val current = users[position]
        holder.userItemView.text = current.namaLengkap
    }
}