package com.yogaadi.dompet.viewmodel

import android.app.Application
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.yogaadi.dompet.model.Transaction
import com.yogaadi.dompet.model.repository.AppDatabase
import com.yogaadi.dompet.model.repository.TransactionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TransactionViewModel(application: Application) : AndroidViewModel(application){
    private val repository : TransactionRepository
    val allTransaction : LiveData<List<Transaction>>
    private val logTAG = "UserViewModel"

    init {
        val trasactionDAO = AppDatabase.getDatabase(application, viewModelScope).transactionDAO()
        repository = TransactionRepository(trasactionDAO)
        allTransaction = repository.allTransaction
    }

    fun insert(transaction: Transaction) = viewModelScope.launch(Dispatchers.IO){
        try {
            repository.insert(transaction)
        }catch (ex : SQLiteConstraintException){
            Log.d(logTAG, ex.message)
        }
    }
}