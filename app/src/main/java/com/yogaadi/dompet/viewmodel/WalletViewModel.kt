package com.yogaadi.dompet.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.yogaadi.dompet.model.Wallet
import com.yogaadi.dompet.model.repository.AppDatabase
import com.yogaadi.dompet.model.repository.WalletRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WalletViewModel (application: Application) : AndroidViewModel(application){
    private val repository : WalletRepository
    val allWallet : LiveData<List<Wallet>>

    init {
        val walletDao = AppDatabase.getDatabase(application, viewModelScope).walletDAO()
        repository = WalletRepository(walletDao)
        allWallet = repository.allWallet
    }

    fun insert(wallet : Wallet) = viewModelScope.launch(Dispatchers.IO){
        repository.insert(wallet)
    }
}