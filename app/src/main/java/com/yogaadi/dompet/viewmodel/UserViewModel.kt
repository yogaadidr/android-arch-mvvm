package com.yogaadi.dompet.viewmodel

import android.app.Application
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.yogaadi.dompet.model.User
import com.yogaadi.dompet.model.repository.AppDatabase
import com.yogaadi.dompet.model.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel (application: Application) : AndroidViewModel(application){
    private val repository : UserRepository
    val allUser : LiveData<List<User>>
    private val logTAG = "UserViewModel"

    init {
        val userDao = AppDatabase.getDatabase(application, viewModelScope).userDao()
        repository = UserRepository(userDao)
        allUser = repository.allUser
    }

    fun insert(user : User) = viewModelScope.launch(Dispatchers.IO){
        try {
            repository.insert(user)
        }catch (ex : SQLiteConstraintException){
            Log.d(logTAG, ex.message)
        }
    }
}