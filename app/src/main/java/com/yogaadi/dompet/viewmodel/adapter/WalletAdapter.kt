package com.yogaadi.dompet.viewmodel.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yogaadi.dompet.R
import com.yogaadi.dompet.model.User
import com.yogaadi.dompet.model.Wallet

class WalletAdapter internal constructor(
    context : Context
) : RecyclerView.Adapter<WalletAdapter.UserHolder>(){

    private val inflater : LayoutInflater = LayoutInflater.from(context)
    private var wallet = emptyList<Wallet>()

    inner class UserHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val userItemView : TextView = itemView.findViewById(R.id.text_list_wallet_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserHolder {
        val itemView = inflater.inflate(R.layout.list_wallet, parent, false)
        return UserHolder(itemView)
    }

    override fun getItemCount(): Int {
        return wallet.size
    }

    internal fun setWallet(words: List<Wallet>) {
        this.wallet = words
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: UserHolder, position: Int) {
        val current = wallet[position]
        holder.userItemView.text = current.walletName
    }
}