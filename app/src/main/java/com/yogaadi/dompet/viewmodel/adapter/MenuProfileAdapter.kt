package com.yogaadi.dompet.viewmodel.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yogaadi.dompet.R
import com.yogaadi.dompet.view.ListWalletActivity

class MenuProfileAdapter internal constructor(
    val context: Context
) : RecyclerView.Adapter<MenuProfileAdapter.UserHolder>() {

    private val inflater : LayoutInflater = LayoutInflater.from(context)
    private var menu = emptyList<String>()

    inner class UserHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val userItemView : TextView = itemView.findViewById(R.id.text_list_text)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuProfileAdapter.UserHolder {
        val itemView = inflater.inflate(R.layout.list_text, parent, false)
        return UserHolder(itemView)
    }

    override fun getItemCount(): Int {
        return menu.size
    }

    internal fun setData(words: ArrayList<String>) {
        this.menu = words
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: UserHolder, position: Int) {
        holder.userItemView.text = menu[position]
        holder.userItemView.setOnClickListener {
            if (position == 0){
                val intent = Intent(context, ListWalletActivity::class.java)
                context.startActivity(intent)
            }
        }
    }


}
