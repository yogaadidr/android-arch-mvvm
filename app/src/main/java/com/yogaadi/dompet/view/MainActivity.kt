package com.yogaadi.dompet.view

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.yogaadi.dompet.R
import com.yogaadi.dompet.view.fragment.HomeFragment
import com.yogaadi.dompet.view.fragment.ProfileFragment

class MainActivity : AppCompatActivity() {

    private lateinit var textMessage: TextView
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                setFragment(HomeFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_add_transaction -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                setFragment(ProfileFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        setFragment(HomeFragment())
    }

    private fun setFragment(fragment : Fragment){
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragment_layout, fragment)
        ft.commit()
    }
}
