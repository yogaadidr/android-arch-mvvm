package com.yogaadi.dompet.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yogaadi.dompet.R
import com.yogaadi.dompet.model.User
import com.yogaadi.dompet.viewmodel.UserViewModel
import com.yogaadi.dompet.viewmodel.adapter.UserAdapter

import kotlinx.android.synthetic.main.activity_basic.*

class BasicActivity : AppCompatActivity() {

    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basic)
        setSupportActionBar(toolbar)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)


        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = UserAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        userViewModel.allUser.observe(this, Observer {users ->
            users?.let { adapter.setUsers(users) }
        })


        fab.setOnClickListener { view ->
           // val intent = Intent(this@BasicActivity, NewUserActivity::class.java)
            //startActivityForResult(intent, newUserRequestCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newUserRequestCode && resultCode == Activity.RESULT_OK) {
            data?.let {
                val user = User(it.getStringExtra("username"),"123456",it.getStringExtra("fullName"))
                userViewModel.insert(user)
            }
        } else {
            Toast.makeText(this@BasicActivity, R.string.alert_input_check,Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        const val newUserRequestCode = 1
    }

}
