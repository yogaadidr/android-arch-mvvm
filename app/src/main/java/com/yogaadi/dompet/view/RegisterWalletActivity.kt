package com.yogaadi.dompet.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.yogaadi.dompet.R
import com.yogaadi.dompet.model.Wallet
import com.yogaadi.dompet.viewmodel.WalletViewModel
import kotlinx.android.synthetic.main.activity_register_wallet.*



class RegisterWalletActivity : AppCompatActivity() {
    private lateinit var walletViewModel: WalletViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_wallet)

        walletViewModel = ViewModelProviders.of(this).get(WalletViewModel::class.java)

        buttonWalletNext.setOnClickListener {
            val walletName = editTextWalletName.text.toString()
            if (TextUtils.isEmpty(walletName)){
                Toast.makeText(this@RegisterWalletActivity,R.string.alert_input_check,Toast.LENGTH_SHORT).show()
            }else{
//                val date = Date(System.currentTimeMillis())
                val wallet = Wallet(1,"Dompet",null,"112")
                walletViewModel.insert(wallet)

            }
        }

        walletViewModel.allWallet.observe(this, androidx.lifecycle.Observer {
            if (it.isNotEmpty()){
                val intent = Intent(this@RegisterWalletActivity, MainActivity::class.java)
                startActivity(intent)
                this.finish()
            }
        })
    }
}
