package com.yogaadi.dompet.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.yogaadi.dompet.R
import com.yogaadi.dompet.model.User
import com.yogaadi.dompet.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {
    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        buttonLoginNext.setOnClickListener {
            val name = editTextLoginName.text.toString()
            val username = editTextLoginUsername.text.toString()

            if(TextUtils.isEmpty(name) || TextUtils.isEmpty(username)){
                Toast.makeText(this@RegisterActivity, R.string.alert_input_check,Toast.LENGTH_SHORT).show()
            }else{
                val user = User(username,"123456",name)
                userViewModel.insert(user)
            }
        }

        userViewModel.allUser.observe(this, Observer {users ->
            users?.let {
                if (it.isNotEmpty()){
                    val intent = Intent(this@RegisterActivity, RegisterWalletActivity::class.java)
                    startActivity(intent)
                    this.finish()
                }
            }
        })
    }
}
