package com.yogaadi.dompet.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.yogaadi.dompet.R
import com.yogaadi.dompet.viewmodel.UserViewModel
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SplashScreen : AppCompatActivity(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        userViewModel.allUser.observe(this, Observer{
            if (it.isEmpty()){
                redirectToRegister()
            }else{
                redirectToMain()
            }
        })
    }

    private fun redirectToRegister(){
        launch {
            delay(3000)
            withContext(Dispatchers.Main){
                val intent = Intent(this@SplashScreen, RegisterActivity::class.java)
                startActivity(intent)
                this@SplashScreen.finish()
            }
        }
    }
    private fun redirectToMain(){
        val intent = Intent(this@SplashScreen, MainActivity::class.java)
        startActivity(intent)
        this@SplashScreen.finish()
    }
}
