package com.yogaadi.dompet.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yogaadi.dompet.R
import com.yogaadi.dompet.viewmodel.WalletViewModel
import com.yogaadi.dompet.viewmodel.adapter.WalletAdapter

class ListWalletActivity : AppCompatActivity() {
    private lateinit var walletViewModel: WalletViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_wallet)
        walletViewModel = ViewModelProviders.of(this).get(WalletViewModel::class.java)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val adapter = WalletAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.isNestedScrollingEnabled = false

        walletViewModel.allWallet.observe(this, Observer {wallet ->
            wallet?.let {
                adapter.setWallet(it)
            }
        })
    }
}
